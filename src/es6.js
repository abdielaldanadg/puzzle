const div = document.getElementsByClassName('box')
for (let i = 0; i < div.length; i++) {
  div[i].addEventListener('click', (event) => {
    const box = event.target
    const x1 = box.getAttribute('x')
    const y1 = box.getAttribute('y')
    const val1 = box.getAttribute('value')
    let noTextAct = document.querySelector(`[y="${y1}"][x="${x1}"] > p`)
    verify(box, x1, y1, val1, noTextAct)
  })
}

const procces = (val1, noTextAct, coorAct, x2, y2) => {
  let noTextCom = document.querySelector(`[y="${y2}"][x="${x2}"] > p`)
  let coorCom = document.querySelector(`[y="${y2}"][x="${x2}"]`)
  let val2 = coorCom.getAttribute('value')
  let val3 = val1 - val2
  if (val3 == val1) {
    noTextAct.innerHTML = parseInt(val2 + 9)
    noTextCom.innerHTML = val1
    coorAct.setAttribute('value', val2)
    coorCom.setAttribute('value', val1)
  }
}

const verify = (box, x1, y1, val1, noTextAct) => {
  const coorAct = box

  let x2 = parseInt(x1)
  let y2 = parseInt(y1) - 1
  if (y2 !== 0) {
    procces(val1, noTextAct, coorAct, x2, y2)
  }

  let x2 = parseInt(x1) + 1
  let y2 = parseInt(y1)
  if (x2 !== 4) {
    procces(val1, noTextAct, coorAct, x2, y2)
  }

  let x2 = parseInt(x1)
  let y2 = parseInt(y1) + 1
  if (y2 !== 4) {
    procces(val1, noTextAct, coorAct, x2, y2)
  }

  let x2 = parseInt(x1) - 1
  let y2 = parseInt(y1)
  if (x2 !== 0) {
    procces(val1, noTextAct, coorAct, x2, y2)
  }
}


